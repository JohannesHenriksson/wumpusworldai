package wumpusworld;

import java.util.Arrays;

/**
 * Contains starting code for creating your own Wumpus World agent. Currently
 * the agent only make a random decision each turn.
 *
 * @author Johan Hagelbäck
 */
public class MyAgent implements Agent {

    private World w;
    int rnd;
    private float alphaValue;
    private float gammaValue;
    //QValue PosX PosY Action
    public float[][][] qValues;
    
    
    private static final int A_MOVE_UP = 0;
    private static final int A_MOVE_RIGHT = 1;
    private static final int A_MOVE_DOWN = 2;
    private static final int A_MOVE_LEFT = 3;
    
    /**
     * Creates a new instance of your solver agent.
     *
     * @param world Current world state
     */
    public MyAgent(World world) {
        w = world;
        alphaValue = 0.5f;//Learning rate
        gammaValue = 0.9f;//Discount factor(Seek reward faster)
        
        int size = world.getSize();
        //8 Different actions, move in every direction and shoot followed by move
        qValues = new float[size][size][8];
        for(int c = 0; c < size; c++)
        {
            //We should never move against the edge of the map
            qValues[0][c][A_MOVE_LEFT] = -Float.MAX_VALUE;
            qValues[0][c][A_MOVE_LEFT + 4] = -Float.MAX_VALUE;
            qValues[c][0][A_MOVE_DOWN] = -Float.MAX_VALUE;
            qValues[c][0][A_MOVE_DOWN + 4] = -Float.MAX_VALUE;
            qValues[size - 1][c][A_MOVE_RIGHT] = -Float.MAX_VALUE;   
            qValues[size - 1][c][A_MOVE_RIGHT + 4] = -Float.MAX_VALUE;           
            qValues[c][size - 1][A_MOVE_UP] = -Float.MAX_VALUE;      
            qValues[c][size - 1][A_MOVE_UP + 4] = -Float.MAX_VALUE;         
        }
    }

    public void setWorld(World w)
    {
        this.w = w;
    }
    /**
     * Asks your solver agent to execute an action.
     */
    public void doAction() {
        //Location of the player
        int cX = w.getPlayerX() - 1;
        int cY = w.getPlayerY() - 1;

        int action = 0;

        //Find best move
        float bestValue = -Float.MAX_VALUE;
        for (int c = 0; c < 8; c++) {
            if (qValues[cX][cY][c] > bestValue && (c < 4 || (w.hasArrow() && w.hasStench(w.getPlayerX(), w.getPlayerY())))) {
                bestValue = qValues[cX][cY][c];
                action = c;
            }
        }
        
        int scoreBefore = w.getScore();
        //Do action: rotate, shoot, move, pickup gold, climb
        this.Rotate(action);
        if(action > 3)
            w.doAction(World.A_SHOOT);
        w.doAction(World.A_MOVE);

        boolean foundGold = false;
        if(w.hasGlitter(w.getPlayerX(), w.getPlayerY()))
        {
            w.doAction(World.A_GRAB);
            foundGold = true;
        }
        else if(w.isInPit())
            w.doAction(World.A_CLIMB);
        
        int scoreAfter = w.getScore();
        int scoreDiff = scoreAfter - scoreBefore;
        if(foundGold)
            scoreDiff += 100.f;
        
        //Update Q-Value
        float nextBestQValue = -Float.MAX_VALUE;
        int newX = w.getPlayerX() - 1;
        int newY = w.getPlayerY() - 1;
        for(int c = 0; c < 8; c++)
        {
            if(qValues[newX][newY][c] > nextBestQValue)
                 nextBestQValue = qValues[newX][newY][c];
        }
        qValues[cX][cY][action] += alphaValue * (scoreDiff + gammaValue * nextBestQValue - qValues[cX][cY][action]);
    }

    //Rotates player to new direction, shortest path
    public void Rotate(int newDir)
    {
        int currentDir = w.getDirection();
        int diff = (newDir - currentDir + 4) % 4;
        switch (diff) {
            case 1:
                w.doAction(World.A_TURN_RIGHT);
                break;
            case 2:
                w.doAction(World.A_TURN_RIGHT);
                w.doAction(World.A_TURN_RIGHT);
                break;
            case 3:
                w.doAction(World.A_TURN_LEFT);
                break;
            default:
                break;
        }
    }
    /**
     * Generates a random instruction for the Agent.
     */  
    public int decideRandomMove() {
        return (int) (Math.random() * 4);
    }

}
